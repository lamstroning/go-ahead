// animate scroll
function navigateMenu(activeElement) {
	document.querySelector('.menu__item_active').classList.remove('menu__item_active');
	activeElement.classList.add('menu__item_active');
}

window.addEventListener('scroll', () => {
	const anchors = document.querySelectorAll('div[id*="anchor-"]');
	const activeElement = [...anchors].filter(element => element.getBoundingClientRect().top < 200).slice(-1)[0] || anchors[0];
	const menuItem = document.querySelector(`a[href*='#${activeElement.id}']`);
	navigateMenu(menuItem);
})

const anchors = document.querySelectorAll('a[href*="#anchor-"]')

for (let anchor of anchors) {
	anchor.addEventListener('click', event => {
		event.preventDefault()
		toggleMenu();
		const blockID = anchor.getAttribute('href').substr(1)

		document.getElementById(blockID).scrollIntoView({
			behavior: 'smooth',
			block: 'center'
		})
	})
}

function toggleDropDown() {
	$('.drop-down').toggleClass('drop-down_active')
}

//toggleMenu

function toggleMenu() {
	document.querySelector('.menu').classList.toggle('menu_opened')
}

//form

function getParams(url) {
	const result = {};
	url.replace(/[?&amp;]+([^=&amp;]+)=([^&amp;]*)/gi, (str, key, value) =>
		result[decodeURIComponent(key)] = decodeURIComponent(value))
	return result
}

$('[type=tel]').mask('+7 (999) 999-9999');

$('.form').on('submit', event => {
	event.preventDefault();
	const $form = $(event.currentTarget);

	$form.addClass('form_ready')

	//Google doc link
	const url = 'https://script.google.com/macros/s/AKfycbxPoay-gvN4tNWAJNcRtcqWGUtijtpklJ02aA_n2NeIznPYaSM/exec';

	$.post(url, $form.serialize(), data => {
		if (data.result == 'success')
			$form.addClass('form_success');
	})
		.fail(data => {
			console.warn("Error! Data: " + data.statusText);
			alert('Ошибка попробуйте еще раз!');
			// HACK - check if browser is Safari - and redirect even if fail b/c we know the form submits.
			if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
				//alert("Browser is Safari -- we get an error, but the form still submits -- continue.");
				$(location).attr('href',redirectUrl);
			}
		})
		.always(() => {
			$form.removeClass('form_ready')
		});
})

//slider design

$(document).ready(() => {
	$('.slider__container.slider__container_design').slick({
		prevArrow: '.slider__arrow.slider__arrow_design.slider__arrow_prev',
		nextArrow: '.slider__arrow.slider__arrow_design.slider__arrow_next',
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: false,
		variableWidth: true,
		autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					variableWidth: false
				}
			}
		]
	});
});

//slider client

$(document).ready(() => {
	$('.slider__container.slider__container_client').slick({
		prevArrow: '.slider__arrow.slider__arrow_client.slider__arrow_prev',
		nextArrow: '.slider__arrow.slider__arrow_client.slider__arrow_next',
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: false,
		autoplaySpeed: 2000,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					variableWidth: false
				}
			}
		]
	});
});
